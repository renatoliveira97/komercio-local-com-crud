from types import MethodType
from flask import Flask, request, jsonify
from environs import Env
from komercio import products, Product

env = Env()
env.read_env()

app = Flask(__name__)


@app.route('/products')
def list_products():
    return jsonify(products), 200
    

@app.route('/products/<int:product_id>')
def get(product_id: int):
    product: list = [item for item in products if item["id"] == product_id]
    return dict(product[0]), 200


@app.post('/products')
def create():
    try:
        data = request.get_json()
        product: Product = Product(**data)
        products.append(product.__dict__)
        return product.__dict__, 201
    except TypeError as e:
        return f'message: {e.args}', 400


@app.route('/products/<int:product_id>', methods=['PUT', 'PATCH'])
def update(product_id: int):
    try:
        data = request.get_json()
        product: Product = Product(**data)
        product.id -= 1
        for i in range(len(products)):
            if products[i]["id"] == product_id:
                products[i] = product.__dict__
        return '', 204
    except TypeError as e:
        return '', 204


@app.delete('/products/<int:product_id>')
def delete(product_id: int):
    global products
    products = [item for item in products if item["id"] != product_id]
    return '', 204
